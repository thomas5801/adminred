<?php 
include __DIR__.'/../xyz/menu.php';


$tmp_ref = "";
if(isset($request[3])){
    $tmp_ref = "../";
}


$EVENT = $_SESSION['user']['Event'];

$SEETLYTOEVA = "N/A";
$JUMLAH = "N/A";


$pageNumber = 1;
$pageSize = 25;


if(isset($_get['pageNumber'])){
    $pageNumber = $_get['pageNumber'];
}

if(isset($_get['pageSize'])){
    $pageSize = $_get['pageSize'];
}

if(isset($_get['search'])){
    $_get['search'] = urldecode($_get['search']);
    $cari = $_get['search'];
    $search = '&search='.$_get['search'];
}else{
    $search = "";
}




?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <title>Rubhez AdminRed</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo $tmp_ref; ?>../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo $tmp_ref; ?>../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo $tmp_ref; ?>../bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo $tmp_ref; ?>../dist/css/AdminLTE.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo $tmp_ref; ?>../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo $tmp_ref; ?>../dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-red-light sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>Admin</b>Red</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Rubhez</b>AdminRed</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
           
          <!-- Notifications: style can be found in dropdown.less -->
          
          <!-- Tasks: style can be found in dropdown.less -->
          
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="../xyz/logout.php" class="dropdown-toggle" >
             
               <span class="hidden-xs">Sign Out</span>
				<i class="fa fa-sign-out-alt"> </i>
            </a>

           </li>   
          <!-- Control Sidebar Toggle Button -->

        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <?php
    

    $menu = str_replace("{{membersdata}}","class='active'",$menu);
    echo $menu;
  
  ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Members
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="dashboard"><i class="fa fa-tachometer-alt"></i> Home</a></li>
        <li class="active">Members Data</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
      
        <!-- left column -->
        <div class="col-md-12 table-responsive">
          

          <!-- /.box -->

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Members Detail</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            
            <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                    <div class="col-sm-2">
                    
                    </div>

                        <!--
                        <div class="dataTables_length" id="example2_length">
                            <label>Show <select name="example2_length" aria-controls="example2" class="form-control input-sm" disabled>
                                <option value="10">10</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select> entries</label>
                             
                        </div>
                        
                        -->
               
            
                    <div class="col-sm-10">
                        <div id="example2_filter" class="dataTables_filter">
                            <form>
                                <input name="pageNumber" type="hidden" value ="<?php echo $pageNumber;?>"> </input>
                                <label>Search By Name : <input value ="<?php if(isset($_get['search'])) echo $_get['search']; ?>" name="search" type="search" class="form-control input-sm" placeholder="" aria-controls="example2"></label>
                            </form>
                        </div>
                    </div>
                </div>
               
                
             


			
             <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Gender</th>
                    <th>Language</th>
                    <th>ID Data</th>
                    <th>Birth Date</th>                    
                    <th>ID Number</th>
                    <th>Nationality</th>
                    <th>Country</th>
                    <th>Provinces</th>
                    <th>City</th>
                    <th>Address</th>
                </tr>
                

                </thead>
                <tbody>
                
                <?php
                    
                    if($EVENT == 3){
                        $EVENT_ = 1;
                        
                    }else{
                        $EVENT_ = $EVENT;
                    }

                    // GET DATA
                    $ch = curl_init(); 
                    
                    
                    if(isset($cari)){
                        $url_ = $titu."api/v1/race/view/members?pageNumber=$pageNumber&pageSize=$pageSize&filter[zmemOwner]=$EVENT_&filter[zmemFullName][like]=%25".urlencode($cari)."%25";
                    }else{
                        $url_ = $titu."api/v1/race/view/members?pageNumber=$pageNumber&pageSize=$pageSize&filter[zmemOwner]=$EVENT_";
                    }

                    // set url
                    curl_setopt($ch, CURLOPT_URL, $url_);

                    // return the transfer as a string 
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
                

                    // $output contains the output string 
                    $output = curl_exec($ch); 

                    // tutup curl 
                    curl_close($ch);      



                    // menampilkan hasil curl
                    $data_all = json_decode($output);
      
                    $data_user = array();
                    if(isset($data_all->linked->zmemAuusId)){
                        foreach($data_all->linked->zmemAuusId as $v){
                            $data_user[$v->id] = $v;
                                
                        }
                    }
                    
                    $data_detail = array();
                    if(isset($data_all->linked->mbsdZmemId)){
                        foreach($data_all->linked->mbsdZmemId as $v){
                            
                            $data_detail[$v->id] = $v;
                            
                                
                        }
                    }
                 
                    
                    if(isset($data_all->data)){
                        
                        foreach($data_all->data as $vall ){
                            echo "<tr>";
                             
                            echo "<td>".$vall->zmemFullName."</td>"; 
                            echo "<td>".$data_user[$vall->links->zmemAuusId]->auusEmail."</td>";  

                            
                            
                            $gender = '<b><p style="color:red">N/A </p></b>';
                            if($vall->zmemGender == 1){
                                $gender = 'MALE';
                            }else if($vall->zmemGender == 2){
                                $gender = 'FEMALE';
                            }
                            echo "<td>".$gender."</td>"; 
                            
                            $language = '<b><p style="color:red">N/A </p></b>';
                            if($vall->zmemLanguage == 1){
                                $language = 'English';
                            }else if($vall->zmemLanguage == 2){
                                $language = 'Indonesia';
                            }
                            echo "<td>".$language."</td>"; 

                             
                            if($EVENT == 3){
                              //  echo "<td>".'<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-warning'.$vall->auusId.'"> Delete</button>'."</td>"; 
                            }else{
                              //  echo "<td>N/A</td>";
                            }
                            
                            if(isset($vall->links->mbsdZmemId[0])){
                                echo "<td> ***".$data_detail[$vall->links->mbsdZmemId[0]]->mbsdId."</td>";
                                echo "<td>".$data_detail[$vall->links->mbsdZmemId[0]]->mbsdBirthPlace.", ".$data_detail[$vall->links->mbsdZmemId[0]]->mbsdBirthDate."</td>";
                            
                                echo "<td>"."**********".substr($data_detail[$vall->links->mbsdZmemId[0]]->mbsdIDNumber,-5)."</td>";

                                echo "<td>".$data_detail[$vall->links->mbsdZmemId[0]]->mbsdNationality."</td>";                                
                                echo "<td>".$data_detail[$vall->links->mbsdZmemId[0]]->mbsdCountry."</td>";
                                echo "<td>".$data_detail[$vall->links->mbsdZmemId[0]]->mbsdProvinces."</td>";
                                echo "<td>".$data_detail[$vall->links->mbsdZmemId[0]]->mbsdCity."</td>"; 

                                if($data_detail[$vall->links->mbsdZmemId[0]]->mbsdAddress <> ""){
                                    echo "<td>".'********'."</td>";
                                }else{
                                    echo "<td>".'<b><p style="color:red">NULL</p></b>'."</td>";
                                }                             
                            
                            }else{
                                echo "<td>".'<b><p style="color:red">NULL</p></b>'."</td>";
                                echo "<td>".'<b><p style="color:red">NULL</p></b>'."</td>"; 
                                echo "<td>".'<b><p style="color:red">NULL</p></b>'."</td>";
                                echo "<td>".'<b><p style="color:red">NULL</p></b>'."</td>";
                                echo "<td>".'<b><p style="color:red">NULL</p></b>'."</td>";
                                echo "<td>".'<b><p style="color:red">NULL</p></b>'."</td>";
                                echo "<td>".'<b><p style="color:red">NULL</p></b>'."</td>";
                                
                                echo "<td>".'<b><p style="color:red">NULL</p></b>'."</td>";
                            }
                            
                           
                            

                            echo "</tr>";
                         
                        }
                    }

                ?>
                
                </tbody>
                <tfoot>
                
                <tr>

                    <th>Name</th>
                    <th>Email</th>
                    <th>Gender</th>
                    <th>Language</th>
                    <th>ID Data</th>
                    <th>Birth Date</th>                    
                    <th>ID Number</th>
                    <th>Nationality</th>
                    <th>Country</th>
                    <th>Provinces</th>
                    <th>City</th>
                    <th>Address</th>

                    
                </tr>
                </tfoot>
              </table>
			  
              
              <!-- INFO -->
              
              <?php
                $total = 0;
                if(isset($data_all->status->totalRecords)){
            
                    $total = $data_all->status->totalRecords;
                
                }
                
                $tmp = $total/25;
                $tmp = ceil($tmp);
                $tmp_ = 0;
                $i = $pageNumber - 2;
                if($i <= 0){
                    $i = 1; 
                }
                
                $max = $pageNumber * $pageSize;
                $min = $max - $pageSize;
                if($max > $total){
                    $max = $total;
                }
                if($min <= 0){
                    $min = 1;
                }
                
              ?>
              <div class="row">
                <div class="col-sm-5">
                   
                   <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">Showing <?php echo  $min;?>  to <?php echo  $max;?> of <?php echo  $total;?> entries</div>

                </div>
              
                <div class="col-sm-7">
                    <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
                        <ul class="pagination">
                        
                        <?php
                        
                            $next = $pageNumber+1;
                            $prev = $pageNumber-1;
                            
                            if($prev <=0){
                                echo "<li class='paginate_button previous disabled'><a>Previous</a></li>";
                            }else{
                                echo "<li class='paginate_button previous'><a href='?pageNumber=$prev$search'>Previous</a></li>";
                            }
                            
                            
  
                            if($i <> 1){
                                echo "<li class='paginate_button'><a href='?pageNumber=1$search'>1</a></li>";
                                echo "<li class='paginate_button'><a>..</a></li>";
                                
                            }
  
                            
                            
                            for($i; $i < $tmp; $i++){
                                if($tmp_ == 5){
                                    echo "<li class='paginate_button'><a>..</a></li>";
                                    break;
                                }
                                
   
                                if($pageNumber == $i){
                                    echo "<li class='paginate_button active'><a href='?pageNumber=$i$search'> $i</a></li>";
                                }else{
                                    echo "<li class='paginate_button'><a href='?pageNumber=$i$search'>$i</a></li>";
                                }
                                
                                $tmp_++;
                                
                            }
                            if($tmp == $pageNumber){
                                echo "<li class='paginate_button active'><a href='?pageNumber=$tmp$search'> $tmp</a></li>";
                            }else{
                                echo "<li class='paginate_button'><a href='?pageNumber=$tmp$search'> $tmp</a></li>";
                            }
                            
                            
                            if($next >  $tmp){
                                echo "<li class='paginate_button next disabled' ><a>Next</a></li>"; 
                            }else{
                                echo "<li class='paginate_button next' ><a href='?pageNumber=$next$search'>Next</a></li>";
                            }
                            
                            
                        ?>

                            
                        </ul>
                    </div>
                </div>
				
			
              </div>
            
            
            </div>
            
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>

      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
    
    
    <div class="modal modal-warning fade" id="modal-warning">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Warning Modal</h4>
              </div>
              <div class="modal-body">
                <p>One fine body&hellip;</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                <a  href="https://www.google.com/" type="button" class="btn btn-outline">Save changes</a>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 0.0.01
    </div>
    <strong>Copyright &copy; 2022 <a href="#">Rubhez</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo $tmp_ref; ?>../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo $tmp_ref; ?>../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?php echo $tmp_ref; ?>../bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo $tmp_ref; ?>../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo $tmp_ref; ?>../dist/js/demo.js"></script>


<!-- DataTables -->
<script src="<?php echo $tmp_ref; ?>../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo $tmp_ref; ?>../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo $tmp_ref; ?>../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>



<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : false,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : false,
      'info'        : false,
      'autoWidth'   : true
    })
  })
  
  
function myFunction() {
  var x = document.getElementById("search");
  x.value = x.value.toUpperCase();
}
  
  
</script>

</body>
</html>
